#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;

use Config::YAML;
use Mojo::Pg;
use Mojo::JSON qw(decode_json encode_json);
use Mojo::Util qw(decode encode);
use Data::Dumper;

binmode(STDOUT, ":utf8");
binmode(STDIN, ":utf8");

my $c = Config::YAML->new(config => '/etc/pgpadrepair.yml');
my @bases = sort keys %{$c->get_bases};

my ($base, $pg, $db, $pad, $value);

main();

sub main {
    if (defined $ARGV[1] && _is_valid_base($ARGV[1], 'name')) {
        $base = $ARGV[1];
    } else {
        _print_bases();
        $base = _choose_base();
    }

    my $addr  = 'postgresql://';
    $addr    .= $c->get_bases->{$base}->{user};
    $addr    .= '@'.$c->get_host;
    $addr    .= ':'.$c->get_port;
    $addr    .= '/'.$base;

    $pg = Mojo::Pg->new($addr);
    $pg->password($c->get_bases->{$base}->{pwd});

    $db = $pg->db;

    $pad = (defined $ARGV[2]) ? $ARGV[2] : _ask_pad();
    $value = _fetch_pad($pad);
    my $head = _need_repair($value);
    if($head) {
        my $rev = _find_good_rev($head);
        repair($head, $rev);
    }
    @ARGV = ();
    main();
}

sub _get_input {
    my $i = <STDIN>;
    if (defined $i) {
        chomp $i;
        return $i;
    } else {
        say "\nGoodbye :-)";
        exit;
    }
}

sub _print_bases {
    my $i = 1;
    say 'Base de données disponibles :';
    for my $base (@bases) {
        say sprintf ' %d) %s', ($i++, $base);
    }
}

sub _choose_base {
    print "\nQuelle base de données souhaitez-vous utiliser ? ";
    my $i = _get_input();
    if (_is_valid_base($i)) {
        say sprintf 'Base de données sélectionnée : %s', ($bases[--$i]);
        return $bases[$i];
    } else {
        say "Vous n'avez pas choisi de base de données existante.";
        _print_bases();
        return _choose_base();
    }
}

sub _is_valid_base {
    my $i = shift;
    my $t = shift || 'number';
    if ($t eq 'name') {
        for (my $j = 0; $j < @bases; $j++) {
            return 1 if $bases[$j] eq $i;
        }
    } else {
        $i--;
        return 1 if ($i >= 0 && $i < @bases);
    }
    return 0;
}

sub _ask_pad {
    print 'Veuillez entrer le nom du pad recherché : ';
    return _get_input();
}

sub _fetch_pad {
    my $p = 'pad:'.shift;
    my $pads = $db->query('SELECT "value" FROM store WHERE "key" = ?', $p)->hashes;

    if ($pads->size == 0) {
        say sprintf "\033[1;31mImpossible de trouver le pad %s\033[1;m", ($p);
        return _ask_pad();
    } elsif ($pads->size > 1) {
        say 'Voilà qui est étrange : j\'ai trouvé plus d\'un pad. Ce n\'est pas sensé arriver. Merci de vérifier la base de données.';
        exit;
    } else {
        return $pads->first->{value};
    }
}

sub _need_repair {
    my $val = shift;
    $val = decode_json(encode 'UTF-8', $val);
    my $head = $val->{head};

    my $q = 'pad:'.$pad.':revs:'.$head;

    if ($db->query('SELECT count(key) FROM store WHERE key = ?', $q)->array->[0] == 0) {
        say sprintf "Le 'head' (valeur %d) du pad %s a effectivement l'air d'être incorrect. Je vais essayer de trouver une révision valide", ($head, $pad);
        return $head;
    } else {
        say sprintf "\033[1;36mLe 'head' (valeur %d) du pad %s (base de données %s) a l'air correct. Désolé, je ne peux rien faire.\033[1;m\n", ($head, $pad, $base);
        return 0;
    }
}
sub _find_good_rev {
    my $rev = shift;
    my $q = 'pad:'.$pad.':revs:'.--$rev;
    if ($db->query('SELECT count(key) FROM store WHERE key = ?', $q)->array->[0] == 0) {
        say sprintf "%d inexistant… Suivant !", ($rev);
        return _find_good_rev($rev);
    } else {
        say sprintf "Gotcha ! La bonne révision semble être %d.", ($rev);
        return $rev;
    }
}

sub repair {
    my $head = shift;
    my $rev = shift;
    my $v = $db->query('SELECT value FROM store WHERE key = ?', 'pad:'.$pad)->hash;
    my $val = decode_json(encode 'UTF-8', $v->{value});
    $val->{head} = $rev;

    say sprintf "\nJe vais modifier la valeur du 'head' du pad %s pour %d.", $pad, $rev;
    print 'Veuillez confirmer : [o/N] ';
    if (_parse_confirm(_get_input())) {
        say 'C\'est parti !';
        my $json = decode 'UTF-8', encode_json($val);
        my $tx = $db->begin;
        $db->query('UPDATE store SET value = ? WHERE key = ?', ($json, 'pad:'.$pad));
        $tx->commit;
        say "\033[1;32mModification effectuée :-)\033[1;m\n";
    } else {
        say "\033[1;36mAbandon. Le pad n'a pas été modifié.\033[1;m\n";
    }
}
sub _parse_confirm {
    my $confirm = shift;
    if ($confirm eq '') {
        return 0;
    } elsif ($confirm =~ m/^yes|no|oui|non|y|n|o$/i) {
        return 1 if ($confirm =~ m/^yes|oui|y|o$/i);
        return 0;
    } else {
        say 'Vous devez écrire yes, oui, y, o, no, non ou n (insensible à la casse)';
        print 'Veuillez confirmer : [o/N] ';
        return _parse_confirm(_get_input());
    }
}
