[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# PAD\_PGHEADREPAIR

At [Framasoft](https://framasoft.org), we host a lot of heavily-used [Etherpad](http://etherpad.org) instances.

The databases are heavily loaded and we often have issues with pads which have a bad head revision: the `head` field in the pad database's record correspond to no revision database's record.

We developped a [plugin](https://framagit.org/framasoft/ep_headrepair/) which verifies that the `head` corresponds to an existing revision record.
If it doesn't, the plugin search the more recent existing revision record and modify the pad's head value.

But we have so many pads (more than 30k on some instances) that the plugin fails to work correctly: it causes a really huge load on the etherpad processus at start and stuck it.

So we developped some scripts to repair the broken pads.

**NB:** As we use PostgreSQL as our etherpad's databases, the scripts works only with PostgreSQL (that's the `PG` in the title of the repo).

## pgpadrepair

`pgpadrepair` asks you for the database and the name of the broken pad, search for a good head and asks you to confirm that you want to fix it.

## auto\_pgpadrepair

`auto_pgpadrepair` asks an etherpad instance for the list of its pads, then check (and fix if needed) all of them.

# Installation

Have a look at the [INSTALL.md file](INSTALL.md)

# License

GNU General Public License, version 3. Check the [LICENSE file](LICENSE).
