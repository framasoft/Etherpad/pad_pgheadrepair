# INSTALLATION

## Dependencies

### Carton

Carton is a Perl dependencies manager, it will get what you need, so don't bother for Perl modules dependencies (but you can read the file `cpanfile` if you want).

```shell
sudo cpan Carton
```

Some modules that Carton will install need to be compiled. So you will need some tools:

```shell
sudo apt-get install build-essential libssl-dev libpq-dev
```

## Installation

After installing Carton:

```shell
cd /opt
git clone https://framagit.org/framasoft/pad_pgheadrepair.git pgheadrepair
cd pgheadrepair
carton install
sudo cp pgpadrepair.yml.template /etc/pgpadrepair.yml
sudo chmod 640 /etc/pgpadrepair.yml
sudo vi /etc/pgpadrepair.yml
```

### pgpadrepair

If you want to have `pgpadrepair` in your `$PATH`:

```shell
sudo ln -s pgpadrepair /usr/sbin/pgpadrepair
```

Then you can use `pgpadrepair` to enter interactive mode.

You can skip the base/pad questions by providing them on the command line:

```shell
pgheadrepair instance
pgheadrepair instance pad
```

### auto\_pgpadrepair

You can see the options of `auto_pgpadrepair` with:

```shell
/opt/pgpadrepair/auto_pgpadrepair -h
```

If you want to start `auto_pgpadrepair` as a systemd service:

```shell
sudo cp auto_pgpadrepair@.service /etc/systemd/system/auto_pgpadrepair@.service
sudo systemctl daemon-reload
# To start auto_pgpadrepair on instance foo:
sudo systemctl start auto_pgpadrepair@foo.service
# To start at boot:
sudo systemctl enable auto_pgpadrepair@foo.service
```

Edit `/etc/systemd/system/auto_pgpadrepair@.service` to change the delay between check loops if you want.
