#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;

use Getopt::Long;
use Config::YAML;
use Mojo::Pg;
use Mojo::Collection 'c';
use Mojo::JSON qw(decode_json encode_json);
use Mojo::Util qw(decode encode);
use Etherpad;
use Data::Dumper;

binmode(STDOUT, ':utf8');
binmode(STDIN, ':utf8');

my $c = Config::YAML->new(config => '/etc/pgpadrepair.yml');

my ($instance, $pg, $db, $pad);
my ($help, $verbose, $daemon, $dry_run, $sleep, $bad) = (0, 0, 0, 0, 600, 0);
shift @ARGV if ($ARGV[0] eq '--');
GetOptions(
    'help|h'       => \$help,
    'instance|i=s' => \$instance,
    'verbose|v'    => \$verbose,
    'daemon|d'     => \$daemon,
    'dry-run|n'    => \$dry_run,
    'sleep|s=i'    => \$sleep,
);

print_usage(0) if $help;
print_usage(1) unless ($instance && $c->{bases}->{$instance});

my $ec = Etherpad->new(
    url    => $c->{bases}->{$instance}->{url},
    apikey => $c->{bases}->{$instance}->{key},
);
print_usage(2) unless $ec->check_token();

if ($daemon) {
    while (1) {
        say 'Launching repair loop' if $verbose;
        head_repair();
        sleep $sleep;
    }
} else {
    say 'Launching repair loop' if $verbose;
    head_repair();
}

sub head_repair {
    my $addr  = 'postgresql://';
    $addr    .= $c->{bases}->{$instance}->{user};
    $addr    .= '@'.$c->{host};
    $addr    .= ':'.$c->{port};
    $addr    .= '/'.$instance;

    $pg = Mojo::Pg->new($addr);
    $pg->password($c->{bases}->{$instance}->{pwd});

    $db = $pg->db;

    say 'Getting pads list' if $verbose;
    my $pads   = c($ec->list_all_pads());
    $pads->each(
        sub {
            $pad = shift;

            if (defined($pad) && $pad ne '') {
                say 'Checking pad '.$pad if $verbose;

                my $value = _fetch_pad($pad);
                if (defined $value) {
                    my $head  = _need_repair($value);
                    if ($head) {
                        my $rev = _find_good_rev($head);
                        repair($head, $rev) if (defined $rev);
                    }
                    sleep 1;
                }
            }
        }
    );
    say sprintf 'Number of bad headed pads: %d', $bad if (!$verbose && $dry_run);
    $bad = 0;
}

sub print_usage {
    my $reason = shift;
    if ($reason == 1) {
        print <<EOF;
You don't have specified any instance or the specified instance is not in the configuration file.

EOF
    } elsif ($reason == 2) {
        print <<EOF;
Unable to contact the instance API. Check your API key in the configuration file and your Etherpad instance.

EOF
    }
    print <<EOF;
auto_pgpadrepair (c) Framasoft 2016 GPLv3

Checks all pads of an Etherpad instance to see if their "head" record is correct and change it if not.
Works only with Etherpad instance using PostgreSQL as database.

Usage: auto_pgpadrepair [--instance|-i <instance>] [--verbose|-v] [--dry-run|-n] [--daemon|-d] [--sleep|-s <delay in seconds>] [--help|-h]

Options:
    --instance|-i <instance>    name of the instance to check
    --verbose|-v                verbose output (by default, auto_pgpadrepair only prints errors)
    --dry-run|-n                check pads, but do nothing
    --daemon|-d                 run in loop indefinitely (by default, auto_pgpadrepair does only one check on all pads and exits)
    --sleep|-s <delay>          delay, in seconds, between loops (works only with --daemon|-d)
    --help|h                    prints this help and exits

Available Etherpad instances:
EOF
    for my $i (keys %{$c->{bases}}) {
        say sprintf '    - %s', $i;
    }
    exit 1;
}

sub _fetch_pad {
    my $p = 'pad:'.shift;
    my $pads = $db->query('SELECT "value" FROM store WHERE "key" = ?', $p)->hashes;

    if ($pads->size == 0) {
        say STDERR sprintf 'Impossible de trouver le pad %s', ($p);
        return undef;
    } elsif ($pads->size > 1) {
        say STDERR sprintf 'Voilà qui est étrange : j\'ai trouvé plus d\'un pad pour %s. Ce n\'est pas sensé arriver. Merci de vérifier la base de données.', ($p);
        return undef;
    } else {
        return $pads->first->{value};
    }
}

sub _need_repair {
    my $val = shift;
    $val = decode_json(encode 'UTF-8', $val);
    my $head = $val->{head};

    my $q = 'pad:'.$pad.':revs:'.$head;

    if ($db->query('SELECT count(key) FROM store WHERE key = ?', $q)->array->[0] == 0) {
        say sprintf 'Le "head" (valeur %d) du pad %s a effectivement l\'air d\'être incorrect. Je vais essayer de trouver une révision valide', ($head, $pad) if $verbose;
        say sprintf 'Bad headed pad: %s', ($pad) if (!$verbose && $dry_run);

        $bad++;

        return $head;
    } else {
        say sprintf 'Le "head" (valeur %d) du pad %s (base de données %s) a l\'air correct.\n', ($head, $pad, $instance) if $verbose;
        return 0;
    }
}
sub _find_good_rev {
    my $rev = shift;
    if ($rev == 0) {
        return undef;
    }
    my $q = 'pad:'.$pad.':revs:'.--$rev;
    if ($db->query('SELECT count(key) FROM store WHERE key = ?', $q)->array->[0] == 0) {
        say sprintf '%d inexistant… Suivant !', ($rev) if $verbose;
        return _find_good_rev($rev);
    } else {
        say sprintf 'Gotcha ! La bonne révision semble être %d.', ($rev) if $verbose;
        return $rev;
    }
}

sub repair {
    my $head = shift;
    my $rev = shift;
    my $v = $db->query('SELECT value FROM store WHERE key = ?', 'pad:'.$pad)->hash;
    my $val = decode_json(encode 'UTF-8', $v->{value});
    $val->{head} = $rev;

    say sprintf 'Je vais modifier la valeur du "head" du pad %s pour %d.', $pad, $rev if $verbose;
    unless ($dry_run) {
        say 'C\'est parti !' if $verbose;
        my $json = decode 'UTF-8', encode_json($val);
        my $tx = $db->begin;
        $db->query('UPDATE store SET value = ? WHERE key = ?', ($json, 'pad:'.$pad));
        $tx->commit;
        say "Modification effectuée :-)\n" if $verbose;
    } else {
        say "Abandon (dry-run). Le pad n'a pas été modifié.\n" if $verbose;
    }
}
